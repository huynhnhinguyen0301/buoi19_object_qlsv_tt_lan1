var dssv = [];

//lấy JSON (đã chuyển và lưu) khi user load lại trang
var getJSON = localStorage.getItem("DSSV_LOCAL");

// nếu lúc đầu người dùng vào trang web, chưa nhập dữ liệu và chưa lưu sẽ không có data JSON để lấy và chuyển đổi lại arr, vì vậy điều kiện là phải khác null thì mới lấy dữ liệu
if (getJSON != null) {
  //convert từ JSON về array
  dssv = JSON.parse(getJSON);
  renderDSSV(dssv);
}
function themSinhVien() {
  // lấy thông tin từ form
  var sv = layThongTinTuForm();

  dssv.push(sv);

  // convert mảng dssv thành JSON để lưu vào localStorage
  var dataJSON = JSON.stringify(dssv);
  console.log("🚀 ~ file: main.js:11 ~ themSinhVien ~ dataJSON:", dataJSON);

  // lưu JSON vào localStorage
  localStorage.setItem("DSSV_LOCAL", dataJSON);

  localStorage
    // renderDSSV
    .renderDSSV(dssv);
}

function xoaSV(id) {
  var viTri = -1;
  for (let index = 0; index < dssv.length; index++) {
    const sv = dssv[index];

    if (sv.ma === id) {
      viTri = index;
    }
  }

  dssv.splice(viTri, 1);
  renderDSSV(dssv);
}
