function renderDSSV(svArr) {
  var contentHTML = "";

  for (let index = 0; index < svArr.length; index++) {
    const sv = svArr[index];
    var contentTr = `
    <tr>
      <td>${sv.ma}</td>
      <td>${sv.ten}</td>
      <td>${sv.email}</td>
      <td>0</td>
      <td>
      <button class="btn btn-danger"  onclick ="xoaSV(${sv.ma})"   >Xóa</button>
      <button class="btn btn-secondary" >Sửa</button>
      </td>
      </tr>`;
    contentHTML = contentHTML + contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
//<td>${sv.tinhDTB().toFixed(2)}</td>

function layThongTinTuForm() {
  //Lấy thông tin từ form
  var ma = document.getElementById("txtMaSV").value * 1;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var diemToan = document.getElementById("txtDiemToan").value * 1;
  var diemLy = document.getElementById("txtDiemLy").value * 1;
  var diemHoa = document.getElementById("txtDiemHoa").value * 1;

  //chuyển thành object
  var sv = {
    ma: ma,
    ten: ten,
    email: email,
    matKhau: matKhau,
    diemToan: diemToan,
    diemLy: diemLy,
    diemHoa: diemHoa,
    tinhDTB: function () {
      var dtb = (this.diemToan + this.diemHoa + this.diemLy) / 3;
      return dtb;
    },
    xepLoai: function () {
      var dtb = this.tinhDTB();
      if (dtb >= 60) {
        return;
      }
    },
  };
  return sv;
}

//local storage + JSON stringtify and JSON parse functions
